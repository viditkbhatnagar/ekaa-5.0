const {
  TeamsActivityHandler,
  CardFactory,
  TeamsInfo,
  MessageFactory,
} = require("botbuilder");
const axios = require("axios");
const querystring = require("querystring");
const { checkTicket } = require("./../apiFunctions/getUserAllTickets");
const { getUserDetails } = require("./../apiFunctions/getUserDetails");
// const { getUserAllTickets} = require("./../apiFunctions/getUserAllTickets");
const AdaptiveCard = require("./../utilities/cards/RestaurantCard.json");
const {
  agentMicroAnalytics,
} = require("./../utilities/cards/agentMicroAnalytics");
const { getAgentDetails } = require("./../apiFunctions/getAgentDetails");
const { getTicketDetails } = require("./../apiFunctions/getTicketDetails");
const {
  fetchTicketStatusOfUser,
} = require("./../apiFunctions/getTicketStatus");
const config = require("../config/config");
const { TaskModulesIds } = require("../models/taskModules/taskmoduleid");
const {
  TaskModulesSubmitIds,
} = require("../models/taskModules/taskmoduleSubmitID");
const {
  TaskModuleResponseFactory,
} = require("../models/taskModules/taskmoduleresponsefactory");
const {
  TaskModuleUIConstants,
} = require("../models/taskModules/taskmoduleuiconstants");
const {
  ticketDetailsCard,
  suggestionsCard,
  updateTicketCard,
  agentNotification,
  feedbackResolveCard,
  statusChangeNotification,
  commentNotification,
  welcomeCardUser,
} = require("../utilities/cards");
const { saveUserFeedback } = require("../apiFunctions/saveUserFeedback");
const {
  sendProactiveMessages,
  saveManagerReference,
  getManagerReference,
  getCallerEmail,
} = require("../services/");
// const { userProfile,agentProfile } = require("../stateManager/");
const { UserProfile } = require("../stateManager/userProfile");
const { AgentProfile } = require("../stateManager/agentProfile");
const { GetTicketStatus } = require("../utilities/constants/button");
// const { logger } = require("../logger/config");

class Bot extends TeamsActivityHandler {
  constructor(conversationState, dispatcher, userState) {
    super();

    if (!conversationState)
      throw new Error(
        "[Bot]: Missing parameter. conversationState is required"
      );
    if (!dispatcher)
      throw new Error("[Bot]: Missing parameter. dispatcher is required");

    this.conversationState = conversationState;
    this.userState = userState;
    this.dispatcher = dispatcher;
    this.baseUrl = config.ticket_App_Ui.baseUrl;
    this.dialogStateAccessor = conversationState.createProperty("DialogState");

    this.AgentProfileAccessor = userState.createProperty("AGENT");
    this.UserProfileAccessor = userState.createProperty("USER");

    this.onMessage(async (context, next) => {
      // console.log();
      console.log(new Date().toString());
      if (
        context.activity.value &&
        context.activity.value.action === "Feedback123"
      ) {
        // let saveFeedback = await saveUserFeedback(context.activity.value);
        await context.sendActivity("Thanks for your feedback");
      } else {
        console.log(context.activity.replyToId);
        await this.dispatcher.run(context, this.dialogStateAccessor);
      }
      await next();
    });
  }

  async handleTeamsTaskModuleFetch(context, taskModuleRequest) {
    try {
      let bot_id = process.env.MicrosoftAppId;

      let agentData = await this.AgentProfileAccessor.get(
        context,
        new AgentProfile()
      );
      let userData = await this.UserProfileAccessor.get(
        context,
        new UserProfile()
      );
      console.log("in bot userdata--------", userData);
      console.log("a----", taskModuleRequest);
      let cardTaskFetchValue = taskModuleRequest.data.data;
      console.log("i am slow------", cardTaskFetchValue);

      let taskInfo = {};
      if (cardTaskFetchValue === TaskModulesIds.CHECK_TICKET_STATUS) {
        console.log("Check Ticket Status");

        taskInfo.url = taskInfo.fallbackUrl =
          this.baseUrl +
          "/" +
          `check_ticket_status?user_id=${userData.USER_ID}&bot_id=${bot_id}`;
        console.log(taskInfo.url);
        this.setTaskInfo(taskInfo, TaskModuleUIConstants.CHECK_TICKET_STATUS);
      } else if (cardTaskFetchValue === TaskModulesIds.RAISE_TICKET) {
        console.log("Raise Ticket");

        taskInfo.url = taskInfo.fallbackUrl =
          this.baseUrl +
          "/" +
          `raiseTicket?user_id=${userData.USER_ID}&bot_id=${bot_id}`;
        console.log(taskInfo.url);
        this.setTaskInfo(taskInfo, TaskModuleUIConstants.RAISE_TICKET);
      } else if (cardTaskFetchValue === TaskModulesIds.SHOW_TICKETS_TO_AGENT) {
        console.log("Show All Ticket to Agent");

        taskInfo.url = taskInfo.fallbackUrl =
          this.baseUrl +
          "/" +
          `getAgentTickets?agent_id=${agentData.AGENT_ID}&bot_id=${bot_id}`;
        console.log(taskInfo.url);
        this.setTaskInfo(taskInfo, TaskModuleUIConstants.SHOW_TICKETS_TO_AGENT);
      }
      return TaskModuleResponseFactory.toTaskModuleResponse(taskInfo);
    } catch (error) {
      console.log(error);
    }
  }

  async handleTeamsTaskModuleSubmit(context, taskModuleRequest) {
    let agentData = await this.AgentProfileAccessor.get(
      context,
      new AgentProfile()
    );
    console.log("inside submit handler-------------");
    console.log(taskModuleRequest);
    let data = taskModuleRequest.data;

    //Raise Ticket Submit Handler
    if (taskModuleRequest.data.taskId === TaskModulesSubmitIds.RAISE_TICKET) {
      context.activity.value.TASK_ID = "RAISE_TICKET";
      let res = data.ticket_details;
      await context.sendActivity({
        attachments: [
          CardFactory.adaptiveCard(
            ticketDetailsCard(
              data.assignment_details.ticket_id,
              res.title,
              res.department,
              res.issue,
              res.description,
              res.issue_observed_on,
              res.impact,
              res.urgency,
              "1"
            )
          ),
        ],
      });
      await context.sendActivity({
        attachments: [CardFactory.adaptiveCard(suggestionsCard())],
      });

      console.log("Agent Email", data.assignment_details.agent_email);
      const fetchAgentManagerReference = await getManagerReference(
        data.assignment_details.agent_email
      );

      if (fetchAgentManagerReference) {
        if (
          fetchAgentManagerReference.userId &&
          fetchAgentManagerReference.convId &&
          fetchAgentManagerReference.aadObjectId &&
          fetchAgentManagerReference.tenantID
        ) {
          let manager_reference = await sendProactiveMessages(
            fetchAgentManagerReference.userId,
            fetchAgentManagerReference.convId,
            fetchAgentManagerReference.aadObjectId,
            fetchAgentManagerReference.tenantID
          );
          console.log("Manager Reference", manager_reference);
          if (manager_reference) {
            const adapter = context.adapter;
            await adapter.continueConversation(
              manager_reference,
              async (context) => {
                let ticket_status = "New";
                await context.sendActivity({
                  attachments: [
                    CardFactory.adaptiveCard(
                      agentNotification(
                        data.assignment_details.agent_name,
                        data.assignment_details.caller_name,
                        data.assignment_details.ticket_id,
                        res.title,
                        res.description,
                        ticket_status
                      )
                    ),
                  ],
                });
              }
            );
          } else {
            console.log("Something Went Wrong ! ");
          }
        } else {
          console.log("No Manager Reference found for the Agent");
        }
      } else {
        console.log(fetchAgentManagerReference.error);
      }

      // await context.sendActivity({
      //   attachments: [CardFactory.adaptiveCard(suggestionCard())],
      // });
    }
    //User Modify Ticket Submit Handler
    else if (
      taskModuleRequest.data.taskId === TaskModulesSubmitIds.MODIFY_TICKET
    ) {
      context.activity.value.TASK_ID = "MODIFY_TICKET";
      let res = data.data;
      await context.sendActivity({
        attachments: [
          CardFactory.adaptiveCard(
            updateTicketCard(
              res.ticket_id,
              res.title,
              res.department,
              res.issue,
              res.description,
              res.issue_observed_on,
              res.impact,
              res.urgency
            )
          ),
        ],
      });
      await context.sendActivity({
        attachments: [CardFactory.adaptiveCard(suggestionsCard())],
      });
    }
    //User Add Comment Submit Handleraget
    else if (
      taskModuleRequest.data.taskId === TaskModulesSubmitIds.USER_ADD_COMMENT
    ) {
      context.activity.value.TASK_ID = "USER_ADD_COMMENT";
      let res = data.data;
      await context.sendActivity("Your Comment Has Been Added Successfully");

      //--------------------- Proactive Message ----------------------------------
      // const data = await blobDataProvider();
      // const managerReference = await sendProactiveMessages(data.userId, data.convId, data.aadObjectId, data.tenantID);
      // // const adapter = stepContext.context.adapter;
      // const adapter = context.context.adapter;
      // // console.log("adapter------>>>>>", JSON.stringify(adapter));
      // await adapter.continueConversation(managerReference, async (context) => {
      //   const message = MessageFactory.attachment(CardFactory.adaptiveCard(agentTicketNotification()));
      //   // const message = MessageFactory.attachment(CardFactory.adaptiveCard(agentTicketNotification(username, res.issue_date, res.title, res.impact, res.urgency, res.description)));
      //   await context.sendActivity('Agent Has Updated Your Ticket State');
      // });
      //------------------------------------------------------------------------------
    }
    //Agent Change Status Submit Handler
    else if (
      taskModuleRequest.data.taskId === TaskModulesSubmitIds.AGENT_CHANGE_STATUS
    ) {
      console.log("in change status .....agent", taskModuleRequest.data);
      // context.activity.value.TASK_ID = "AGENT_CHANGE_STATUS";

      // console.log("date in agent status ", data);
      // let res = data;
      // console.log("Inside Agent Change Status");
      // let caller_email = data.caller_email;
      let ticket_id = data.ticket_details.ticket_id;
      let callerEmail = await getCallerEmail(ticket_id);
      console.log("Caller Details", callerEmail);
      if (callerEmail.status == "Success") {
        let caller_email = callerEmail.email;
        console.log("Caller Email is : ", caller_email);
        const fetchCallerReference = await getManagerReference(caller_email);
        if (fetchCallerReference) {
          if (
            fetchCallerReference.userId &&
            fetchCallerReference.convId &&
            fetchCallerReference.aadObjectId &&
            fetchCallerReference.tenantID
          ) {
            let manager_reference = await sendProactiveMessages(
              fetchCallerReference.userId,
              fetchCallerReference.convId,
              fetchCallerReference.aadObjectId,
              fetchCallerReference.tenantID
            );
            console.log("Manager Reference", manager_reference);
            if (manager_reference) {
              const adapter = context.adapter;
              await adapter.continueConversation(
                manager_reference,
                async (context) => {
                  // const message = MessageFactory.attachment(
                  //   CardFactory.adaptiveCard(
                  //     agentTicketNotification(
                  //       username,
                  //       res.issue_date,
                  //       res.title,
                  //       res.impact,
                  //       res.urgency,
                  //       res.description
                  //     )
                  //   )
                  // );
                  let notes;
                  console.log(data.ticket_details.ticket_cancellation_notes);
                  if (data.ticket_details.ticket_state === "Cancelled") {
                    notes = data.ticket_details.ticket_cancellation_notes;
                  } else if (data.ticket_details.ticket_state === "Resolved") {
                    notes = data.ticket_details.ticket_resolution_notes;
                  } else if (data.ticket_details.ticket_state === "On Hold") {
                    notes = data.ticket_details.ticket_on_hold_reason;
                  } else {
                    notes =
                      "Hi user, your ticket is in progress . Your issue will resolve soon.";
                  }
                  console.log("Notes", notes);
                  if (data.ticket_details.ticket_state !== "Resolved") {
                    await context.sendActivity({
                      attachments: [
                        CardFactory.adaptiveCard(
                          statusChangeNotification(
                            `${agentData.AGENT_F_NAME} ${agentData.AGENT_L_NAME}`,
                            data.ticket_details.ticket_id,
                            data.ticket_details.ticket_title,
                            data.ticket_details.title_description,
                            data.ticket_details.ticket_state,
                            notes
                          )
                        ),
                      ],
                    });
                  } else {
                    await context.sendActivity({
                      attachments: [
                        CardFactory.adaptiveCard(
                          feedbackResolveCard(
                            `${agentData.AGENT_F_NAME} ${agentData.AGENT_L_NAME}`,
                            data.ticket_details.ticket_id,
                            data.ticket_details.ticket_title,
                            data.ticket_details.title_description,
                            data.ticket_details.ticket_state,
                            notes
                          )
                        ),
                      ],
                    });
                  }
                }
              );
            } else {
              console.log("Something Went Wrong ! ");
            }
          } else {
            console.log("No Manager Reference found for the Agent");
          }
        } else {
          console.log(fetchCallerReference.error);
        }
      } else {
      }

      await context.sendActivity("Ticket state is updated successfully");
      //});
      //----------------------------------------------------------------------
    } else if (
      taskModuleRequest.data.taskId === TaskModulesSubmitIds.AGENT_ADD_COMMENT
    ) {
      context.activity.value.TASK_ID = "AGENT_ADD_COMMENT";
      let ticket_id = data.data.ticket_details.ticket_id;
      let callerEmail = await getCallerEmail(ticket_id);
      if (callerEmail.status == "Success") {
        const fetchCallerReference = await getManagerReference(
          callerEmail.email
        );
        if (fetchCallerReference) {
          if (
            fetchCallerReference.userId &&
            fetchCallerReference.convId &&
            fetchCallerReference.aadObjectId &&
            fetchCallerReference.tenantID
          ) {
            let manager_reference = await sendProactiveMessages(
              fetchCallerReference.userId,
              fetchCallerReference.convId,
              fetchCallerReference.aadObjectId,
              fetchCallerReference.tenantID
            );
            console.log("Manager Reference", manager_reference);
            if (manager_reference) {
              const adapter = context.adapter;
              await adapter.continueConversation(
                manager_reference,
                async (context) => {
                  await context.sendActivity({
                    attachments: [
                      CardFactory.adaptiveCard(
                        commentNotification(
                          `${agentData.AGENT_F_NAME} ${agentData.AGENT_L_NAME}`,
                          data.data.ticket_details.ticket_id,
                          data.data.ticket_details.ticket_title,
                          data.data.ticket_details.ticket_descritpion,
                          data.data.comment
                        )
                      ),
                    ],
                  });
                }
              );
            } else {
              console.log("Something Went Wrong ! ");
            }
          } else {
            console.log("No Manager Reference found for the Agent");
          }
        } else {
          console.log(fetchCallerReference.error);
        }
      } else {
        console.log("No Caller Email Found");
      }

      await context.sendActivity("Your Comment Has Been Added Successfully");
    }
  }

  setTaskInfo(taskInfo, uiSettings) {
    taskInfo.height = uiSettings.height;
    taskInfo.width = uiSettings.width;
    taskInfo.title = uiSettings.title;
  }

  // async handleTeamsMessagingExtensionSubmitAction(context, action) {
  //   switch (action.commandId) {
  //   case 'createCard':
  //       return  empDetails();

  //   }
  //   function createCardCommand(context, action) {
  //     const data = action.data;
  //     const heroCard = CardFactory.heroCard(data.title, data.text);
  //     heroCard.content.subtitle = data.subTitle;
  //     const attachment = { contentType: heroCard.contentType, content: heroCard.content, preview: heroCard };
  //     return {
  //         composeExtension: {
  //             type: 'result',
  //             attachmentLayout: 'list',
  //             attachments: [
  //                 attachment
  //             ]
  //         }
  //     };
  //   }
  // }

  async handleTeamsMessagingExtensionFetchTask(context, action) {
    switch (action.commandId) {
      case "FetchRoster":
        return empDetails();
      case "Static HTML":
        return ticketStatus();
    }
    function empDetails() {
      return {
        task: {
          type: "continue",
          value: {
            width: 730,
            height: 700,
            title: "Task module WebView",
            url: `https://dev-ekaaui.azurewebsites.net/raiseTicket?user_id=USR4&bot_id=db647f07-5c49-4be1-a55c-3d716642f01f`,
          },
        },
      };
    }

    function ticketStatus() {
      return {
        task: {
          type: "continue",
          value: {
            width: 730,
            height: 700,
            title: "Task module WebView",
            url: `https://dev-ekaaui.azurewebsites.net/check_ticket_status?user_id=USR4&bot_id=db647f07-5c49-4be1-a55c-3d716642f01f`,
          },
        },
      };
    }
  }

  async handleTeamsMessagingExtensionQuery(context, query) {
    const searchQuery = query.parameters[0].value;
    console.log("search=>", searchQuery);
    const attachments = [];
    let response;
    if (
      searchQuery.slice(0, 4).trim().toLowerCase() == "tick" &&
      searchQuery.length >= 5
    ) {
      const member = await TeamsInfo.getMember(
        context,
        context.activity.from.id
      );
      console.log("***************", member);
      const newemail = member.email;
      const userbyemail = await axios.get(
        `https://ticketappekaa-dev.azurewebsites.net/User/getUser/${newemail}/`
      );
      const usrid = userbyemail.data.data.user_id;

      console.log("User id !!!!----------------->", usrid);
      let userData = await this.UserProfileAccessor.get(
        context,
        new UserProfile(usrid)
      );

      const response = await checkTicket(
        userData.USER_ID,
        searchQuery.toUpperCase()
      );

      if (response) {
        // console.log("INSIDE BOT",response);
        console.log(`These are Your Ticket Details
        
        Ticket Id : ${response.ticket_id}
        Ticket Title : ${response.title}
        Ticket Department : ${response.department}
        Ticket Description : ${response.description}
        Ticket Caller : ${response.caller}
      
        `);
      } else {
        console.log("No tickets exist");
      }
      console.log("---------------------------------");
      console.log(searchQuery.toUpperCase());
      console.log("---------------------------------");
      // console.log(response);
      // return ;

      const preview = CardFactory.thumbnailCard(
        `Click to get your ticket details`,
        `${searchQuery.toUpperCase()}`
      );

      const adaptive = CardFactory.adaptiveCard(
        ticketDetailsCard(
          response.ticket_id,
          response.title,
          response.department,
          response.issue,
          response.description,
          response.issue_observed_on,
          response.impact,
          response.urgency,
          "1"
        )
      );

      const attachment = { ...adaptive, preview };

      return {
        composeExtension: {
          type: "result",
          attachmentLayout: "list",
          attachments: [attachment],
        },
      };
    }

    if (
      searchQuery.trim().toLowerCase() == "harshit.sharma@celebaltech.com" &&
      searchQuery.length >= 5
    ) {
      response = await getUserDetails(searchQuery);
      console.log("---------------------------------");
      console.log(searchQuery.toUpperCase());
      console.log("---------------------------------");
      console.log(response);

      const heroCard = CardFactory.heroCard(
        response.data.data.user_id,
        response.data.data.department
      );
      const preview = CardFactory.heroCard(
        response.data.data.user_id,
        response.data.data.department
      );

      preview.content.tap = { type: "invoke", value: {} };
      const attachment = { ...heroCard, preview };
      attachments.push(attachment);
    }

    if (
      searchQuery.trim().toLowerCase() == "harshit.sharma@celebaltech.com" &&
      searchQuery.length >= 5
    ) {
      response = await getAgentDetails(searchQuery);
      console.log("---------------------------------");
      console.log(searchQuery.toUpperCase());
      console.log("---------------------------------");
      console.log(response);

      const heroCard = CardFactory.heroCard(response.data.data.agent_id);
      const preview = CardFactory.heroCard(response.data.data.agent_id);

      preview.content.tap = { type: "invoke", value: {} };
      const attachment = { ...heroCard, preview };
      attachments.push(attachment);
    }

    return {
      composeExtension: {
        type: "result",
        attachmentLayout: "list",
        attachments: attachments,
      },
    };
  }

  async handleTeamsMessagingExtensionSelectItem(context, obj) {
    return {
      composeExtension: {
        type: "result",
        attachmentLayout: "list",
        attachments: [CardFactory.thumbnailCard(obj.description)],
      },
    };
  }
}
module.exports.Bot = Bot;
