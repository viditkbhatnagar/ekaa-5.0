module.exports = {
    AgentDispatch :require('./AgentDispatch').AgentDispatcher,
    AgentMainD : require('./mainAgentDialog').AgentMainDialog,
    AgentMicroAnalytics : require('./microAnalyticsDialog').MicroAnalyticsDialog,
    AgentInterruptDialog: require('./interruptDialog').CancelAndHelpDialog
}