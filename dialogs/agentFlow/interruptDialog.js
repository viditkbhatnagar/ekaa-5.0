// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const { InputHints } = require("botbuilder");
const { ComponentDialog, DialogTurnStatus } = require("botbuilder-dialogs");
const { ButtonConst,DailogConst } = require("../../utilities/constants");

/**
 * This base class watches for common phrases like "help" and "cancel" and takes action on them
 * BEFORE they reach the normal bot logic.
 */
class CancelAndHelpDialog extends ComponentDialog {
    async onContinueDialog(innerDc) {
        try {
            const result = await this.interrupt(innerDc);
            if (result) {
                return result;
            }
            return await super.onContinueDialog(innerDc);
        } catch (err) {
            console.log(err);
        }
    }
    async interrupt(innerDc) {
        try {
            console.log("interrrupt inside agentFlow====",innerDc.context.activity)
            innerDc.context.activity.value
                ? (innerDc.context.activity.text =
                    innerDc.context.activity.value.action ?innerDc.context.activity.value.action : innerDc.context.activity.text)
                : (innerDc.context.activity.text =innerDc.context.activity.text);
            if (innerDc.context.activity.text) {
                let text = innerDc.context.activity.text
    
                switch (text) {
                    // case "help":
                    // case "?": {
                    //     const helpMessageText = "How may i help you ? Please describe your problem below and hit send.";
                    //     await innerDc.context.sendActivity(
                    //         helpMessageText,
                    //         helpMessageText,
                    //         InputHints.ExpectingInput
                    //     );
                    //     return { status: DialogTurnStatus.waiting };
                    // }
                    //  case "quit": {
                    //      console.log("quit is working......")
                    //     return await innerDc.cancelAllDialogs();
                    //  }
                    // case "hi": 
                    // case "hello": 
                    // case "Hi": 
                    case "#USER": 
                    case "#User": 
                    case "#user": 
                        {
                        await innerDc.cancelAllDialogs();
                        return innerDc.replaceDialog(DailogConst.USER_MAIN_DIALOG);
                    }
                    case "#Agent" :
                    case "#AGENT" :
                    case "#agent": {
                        console.log('inside userflow interrupt dialog, doing as expected======');
                    await innerDc.cancelAllDialogs();
                    return await innerDc.beginDialog(DailogConst.AGENT_MAIN_DIALOG);
                    }
                    // case ButtonConst.AskAQuestion: {
                    //     await innerDc.cancelAllDialogs();
                    //     return await innerDc.beginDialog(DailogConst.ASK_A_QUESTION_DIALOG);
                    // }

                  
       
                }
            }
        } catch (err) {
            console.log(err);
        }
    }
}

module.exports.CancelAndHelpDialog = CancelAndHelpDialog;
