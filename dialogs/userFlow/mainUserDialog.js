const { WaterfallDialog, ComponentDialog } = require("botbuilder-dialogs");
const { welcomeCardUser } = require("../../utilities/cards");
const { CardFactory } = require("botbuilder");
const { DailogConst, ButtonConst } = require("../../utilities/constants");
const { AskQuestion, RaiseTicket, CancelAndHelpDialog,CheckTicketStatus} = require("./index");
const { LuisRecognizer } = require("botbuilder-ai");
const { luisConfiguration } = require("../../config/config");


const luisConfig = {
  applicationId: luisConfiguration.LuisAppId,
  endpointKey: luisConfiguration.LuisEndpointKey,
  endpoint: luisConfiguration.LuisEndpoint,
};

class UserDialog extends ComponentDialog {
  constructor(conversationState,userState) {
    super(DailogConst.USER_MAIN_DIALOG);
    if(!conversationState){
      throw new Error('[UserDialog]: Missing parameter. conversationState is required');
    }
    if(!userState){
      throw new Error('[UserDialog]: Missing parameter. userState is required');
    }
    this.conversationState = conversationState;
    this.recognizer = new LuisRecognizer(luisConfig, {
      apiVersion: "v3",
    });
    this.addDialog(new AskQuestion(conversationState,userState));
    this.addDialog(new RaiseTicket(conversationState,userState));
    this.addDialog(new CheckTicketStatus(conversationState,userState));
    this.addDialog(
      new WaterfallDialog(DailogConst.USER_MAIN_DIALOG_WF, [
        this.userWelcomeStep.bind(this),
        this.navigationStep.bind(this),
      ])
    );

    this.initialDialogId = DailogConst.USER_MAIN_DIALOG_WF;
  }

  async userWelcomeStep(stepContext) {
    await stepContext.context.sendActivity({
      attachments: [CardFactory.adaptiveCard(welcomeCardUser())],
    });
    return ComponentDialog.EndOfTurn;
  }

  async navigationStep(stepContext) {
    if(stepContext.context.activity.value && stepContext.context.activity.value.action === "Ask Question"){
      return stepContext.beginDialog(DailogConst.ASK_A_QUESTION_DIALOG);
    }else{
      let luisresponse = await this.recognizer.recognize(stepContext.context);
    let luisIntent = luisresponse.luisResult.prediction.topIntent;
    console.log(luisIntent);
    switch (luisIntent.toLowerCase()) {
      case "raiseticket":
        return await stepContext.beginDialog(DailogConst.RAISE_TICKET_DIALOG);
        break;
      case "checkticketstatus":
        return await stepContext.beginDialog(
          DailogConst.CHECK_TICKET_STATUS_DIALOG,
          {
            luisResult: true,
            entities: luisresponse.luisResult.prediction.entities.ticket,
          }
        );
        break;
      // default:
      //   return await stepContext.beginDialog(DailogConst.ASK_A_QUESTION_DIALOG);
    }
    }
  }
}

module.exports.UserDialog = UserDialog;
