const {
  ComponentDialog,
  WaterfallDialog,
} = require("botbuilder-dialogs");
const { TeamsInfo } = require("botbuilder");
const { ActivityTypes,ActionTypes,MessageFactory } = require("botbuilder");
const { suggestionsCard,feedbackForm,feedbackCard,updatedFeedbackForm } = require("../../utilities/cards");
const { DailogConst, ButtonConst } = require("../../utilities/constants");
const { UserProfile } = require("../../stateManager/userProfile");
const { saveUserFeedback } = require("../../apiFunctions/");
const { CardFactory } = require("botbuilder");
// const {AskQuestion} = require('./askQuestion');
let newActivity;
let updatedCard;
let feedbackState;
let message;
class FeedbackDialog extends ComponentDialog {
  constructor(conversationState,userState) {
    super(DailogConst.USER_FEEDBACK_DIALOG);
    if(!conversationState){
      throw new Error("[FeedbackDialog]: Missing parameter. conversationState is required");
    }
    if(!userState){
      throw new Error("[FeedbackDialog]: Missing parameter. userState is required");
    }
    this.conversationState = conversationState;
    this.UserProfileAccessor = userState.createProperty("USER");
    // this.addDialog(new AskQuestion(conversationState, userState));
    this.addDialog(
      new WaterfallDialog(DailogConst.USER_FEEDBACK_DIALOG_WF, [
        this.feedbackStep.bind(this),
        this.feedbackStep2.bind(this),
        this.handleSuggestions.bind(this),
      ])
    );
    this.initialDialogId = DailogConst.USER_FEEDBACK_DIALOG_WF;
  }

  async feedbackStep(stepContext) {
    await stepContext.context.sendActivity({
      attachments: [CardFactory.adaptiveCard(feedbackForm())],
    });
    return ComponentDialog.EndOfTurn;
  }

  async feedbackStep2(stepContext) {
    message = "Thank You for your valuable feedback";
    let feedback_message = stepContext.context.activity.value.feedback;
    const feedback = stepContext.context.activity.text;
    let userData = await this.UserProfileAccessor.get(
      stepContext.context,
      new UserProfile()
    );
    updatedCard = CardFactory.adaptiveCard(
      updatedFeedbackForm()
    ),
    updatedCard.id = stepContext.context.activity.replyToId;
    newActivity =  MessageFactory.attachment(updatedCard);
    newActivity.id = stepContext.context.activity.replyToId;
    await stepContext.context.updateActivity(newActivity);
    const savedFeedback = await saveUserFeedback(userData.USER_EMAIL, feedback_message);
    if (savedFeedback.status == "Success") {
      stepContext.context.sendActivity(message);
      await stepContext.context.sendActivity({
        attachments: [CardFactory.adaptiveCard(suggestionsCard())],
      });
    } else {
      console.log(savedFeedback.error);
    }
    return ComponentDialog.EndOfTurn;
  }

  async handleSuggestions(stepContext) {
    if(stepContext.context.activity.value && stepContext.context.activity.value.action === "Ask Question"){
      return stepContext.beginDialog(DailogConst.ASK_A_QUESTION_DIALOG);
    }else{
      return stepContext.endDialog();
    }
  }
}

module.exports.FeedbackDialog = FeedbackDialog;
