class UserProfile {
    constructor(usrid){
        this.USER_ID = usrid;
        this.USER_EMAIL = null;
        this.USER_F_NAME = null;
        this.USER_L_NAME = null;
    }
}


module.exports = {
    UserProfile : UserProfile,
    UserProfileId : 'UserInfo'
}