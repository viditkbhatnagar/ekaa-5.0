const { BlobServiceClient } = require("@azure/storage-blob");
const config = require("../config/config");

// Function to get data from the stored blob.
module.exports.blobDataProvider = async () => {
  let finalData;
  const agentEmail = "anshuman.ranjan@celebaltech.com";

  const blobServiceClient = BlobServiceClient.fromConnectionString(
    config.blobCreds.AZURE_STORAGE_CONNECTION_STRING
  );
  const containerName = "convref01835750-49f6-11ec-9b88-03518047f399";
  const containerClient = blobServiceClient.getContainerClient(containerName); // Create a unique name for the blob

  const blobName = `${agentEmail}.json`; // Get a block blob client
  const blockBlobClient = containerClient.getBlockBlobClient(blobName);

  for await (const blob of containerClient.listBlobsFlat()) {
    console.log("\t", blob.name);
  }
  const downloadBlockBlobResponse = await blockBlobClient.download(0);
  console.log("\n Downloaded blob content...");

  async function streamToString(readableStream) {
    return new Promise((resolve, reject) => {
      const chunks = [];
      readableStream.on("data", (data) => {
        chunks.push(data.toString());
      });
      readableStream.on("end", () => {
        resolve(chunks.join(""));
      });
      readableStream.on("error", reject);
    });
  }

  let y = await streamToString(downloadBlockBlobResponse.readableStreamBody);
  finalData = JSON.parse(y);

  return finalData;
};
