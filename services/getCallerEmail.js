const axios = require("axios");
const url = "https://ticketappekaa.azurewebsites.net/";

module.exports = {
  getCallerEmail: async (ticket_id) => {
    try {
      const response = await axios.get(`${url}/ticket/${ticket_id}`);
      if (response && response.data) {
        console.log(response.data);
        return {
          status: "Success",
          email: response.data.data.caller_details.caller_email,
          name: response.data.data.caller_details.caller_name,
        };
      }
    } catch (error) {
      if (error.response) {
        console.log(error.response.data);
      }
    }
  },
};
