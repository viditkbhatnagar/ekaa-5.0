module.exports = {
  AskAQuestion: "Ask Question",
  GetTicketStatus: "Get Ticket Status",
  RaiseATicket: "Raise a Ticket",
  showAllTickets: "Show All Tickets",
};
