
//Raise query adaptive card
// const { RaiseQueryImage } = require("../cards/imageBase64.js");
module.exports.RaiseQueryCard = (question, answer) => {
  try {
    let card = 
    {
      "type": "AdaptiveCard",
      "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
      "version": "1.4",
      "body": [
          {
              "type": "ColumnSet",
              "columns": [
                  {
                      "type": "Column",
                      "width": "auto",
                      "items": [
                          {
                              "type": "Image",
                              "url": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhIVFRUVFxUXGBgXFxUXFxYXFxcXGBgaHRUYHSggGB0lHRcXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0lICUtLS0tLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMgAyAMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABwEDBAUGAgj/xABGEAABAgMDBwcICQMEAwEAAAABAAIDEbEEBSEGEjFBUWHwBxMicYGRoTJSU1RyktHhFBUWF0KCk8HxIzNiQ6LS06OywjT/xAAaAQACAwEBAAAAAAAAAAAAAAAABAMFBgIB/8QANBEAAgECAwQIBQQDAQAAAAAAAAECAxEEEiEFMUFRE2FxgZGhsdEUMsHh8BUiQlJicvEj/9oADAMBAAIRAxEAPwCcUREAEREAEREAERaW98prNZsIsQZ3mN6T+4aO2S8clFXZ3CnKcssE2+SN0ije8eU7GUCB+aI7R+VvxXPWzLu2xP8AVEMbGNaPEzcO9LyxVNbtSypbGxM96Ue1/RJk0ooBiX7aX+VaIzp6jEeQPFYptjz/AKj95mVH8auEfMaWwZ8Zrw+6PohF8+wr2jtxbGiN2Se4UKz7Llfbofk2hx258n/+4KFjY8Ucy2FU/jNd917k5oorsHKZGb/ehMcNrZscezEUXU3Tl3ZI0mucYTjqieT74wHbJTxxFOW5iNbZmJpauN11a+mvkdWitw4ocAWkEHQRiD2q4phAIiIAIiIAIiIAIiIAIiIAIiIALS39lJAsg/qum6WDG4vPZqG8rmsrsuxDzoVmILxgYmlrTsaPxHfo69UZ2iO57i97iXOMyXEkz2klJ1sUo6Q19C6wOyJVUp1tFy4v29eO46S/cubTHmGnmoZ0MYTnEf5O0nskuXJx36ylaJSqQlJyd5O5pKVGnSjlpqy/O994pVK0TjqTglcko4JTgBOAErrOzjwQArRKVSlUrRACtE4JTglOAEAbO6L/ALRZjOFELR5mlp62nDuUj5OZfQo0mRgIUTbP+m47ifJ6j3lRLXWdiUqpqdadPdu5CWKwFHE/MrPmt/s+8+jWOBxC9KGclssYtmIY+cSF5utg/wAT+1FLF2XlDtDBEhODgdisqVaNRaGVxmCqYWVpap7nwfs+pmciIpRMIiIAIiIAIiIAKMcvMss4us9mfJgwiRAfK/wafN2kadGic9nyiZTc002aE6URw6bh+Bh1e0fAdYUVn+AkcTXt+yPeaDZOzlJKvVX+q+r+nPfusOAErRK0SlUgaMUqlaJWicEoAcEpwAnACV27EAK6zsSlUpVK0QArROCU4JVOAEAV4ATgnYlduxKVQApVK0StE4J2oApTWdq3GTt/RLJEBaTmnym7RtWo4ATglexk4u6OKlOFSLhNXT/P+PgT7ct7Q7TDD2Ga2SgzJa/nWWIDP+mToU0XfbWxWBzSrajVVSN+PExmOwUsLUy709z/ADivZ8TLREUwkEREAFqMpL3bZLO+M7EjBo8550D9zuBW3UQcpd887aOZaZsgzHXEPlHs8nsKhr1OjhfiO4DC/EVlF7lq+z7nK2q0uiPdEiHOLiXHeTpKs1olaJSqqDapWVkKVSurclaLKu6wRI8RsKE0kng46hvQDaSuzF4JTgBShdnJrCDR9IiPe7zWSDBhomWzd14dS2f3fWHzH++UysJUKuW2sLF2V32L3a9CHK693HglKqY/u9sPmP8Afcn3e2H0b/fK9+EqdRz+t4blLwXuQ5WicEqY/u9sPmP99yfd7YfMf75R8JU6g/W8Nyl4L3Ic4ASus7OPBSte+RVhgwIsXNcMxjnAlzpTAw8ZKKZjsqoalKVNpSHMLjKeJTcE9Oa+7FKpWiVolNZ2qMbHHWnACcAJwSgBXWUpVKVStEAK0Xdcn+UBYeacZgaOr5LhaaztV2yxyxzXt0zmApaVTo5ZhXG4VYmi4ceHb99x9DtMxMa17XO5I3oI0ICeoEdS6JXBh2mnZhERB4a6/bcIFniRj+BpI3u0NHfJQJFeXOJJmSSSd5Un8q1vzYEOCD/ccXO9lg+JB/KospVVuLneeXkarYlHJRdTjJ+S09blaVTjqStEH8lKFyOCVK/JfdQZZzHI6cUkA4YMaZYHeQSeobFFGseAU/3LY+ZgQoWE2Ma0y0Z0uke0zKbwcLzzcil25WcaMaa/k/Jfdo2CIoUyn5QrYLVGZZ4wZDY9zGgQ4Z8kyJJe0mcwdyfnUUN5ncPhp121DhzJrRQB94t5etD9KD/1qv3jXl6z/wCKD/wUXxMRv9Krc4+L9ifkUAfeJefrQ/Sg/wDWq/eLeXrI/Sgf8EfExD9Lq84+L9iS+Va381YS0EgxXshiXa8g7iGEdqhqzvJ66K9fWUdqtmYLRF5zNmWjNY0DOlM9BonoGlWrM3DjFKYiam7ou9m0JUYZXvu2X6aztVeAE4ASus7EqWw4J2KlKqtKpWiAFaJSqpSqrwAgBwAnBOxOCUpVAHZZA3nmOzdQPgfnVS2x0xNQDckfMjNO3DqmptyftXOQWnYJK1ws81O3LQx+16PR4ltbpa+/nqbNERMFYRDyo2rOtQbPBkNo7SS74Ljq0W+y4i51ui65EAdzfitDwSqes71JdpucDHLhqa/xXmr/AFHBKcAJwAlaKIaNrknZOdtkFmnptc7WJM6RHbmy7VPSibknsedaIkSWEOHKe9xkPAOUsqzwkbQvzZk9tVM2Iy8kvPX0aMG+LYIECLGImIcN75bc1pMu2Ul80SLiSSSTi4nSSdJ7VN/KzbebsDmYTivYzTjIHPMtvkgdqheys+S4xUtUhjY9L9kpc36f9LXMn4BOYK2QZ36zs48EzR2VSecvOgRrOY7qqvMHjUtlm99EzB2bdqM4dCjFsd2xIk+bY98pTzWl0p6NHUe5bWFc0cD+xE3DMf8ABSdyUWIMsr4shOLEOOvNZgAfzZ5/Mu4TUMNnim2UtbaroVpQjFOztc+ffqiP6KLPbmP+CfVMf0EWXsPx8F9BIuvglzOP16f9F4s+ffqiP6GJP2H4eCfVEf0MXf0HY+C+gkR8Ev7B+vT/AKLxPn36oj+hibhmP+CfVMf0MXecx3wX0EsG97cIECJFMugxzsdBIGA7TIdqHgo8wW3aj0VNeLPn9wlgRKWEtcwqVovIjTM5zJXqlVXmkTPcF8iDvB69al/Ie0zaWqHh/AUl5AxukN7RRPYJ/MuwoNvR0py7V6P3JDRET5nCBsrP/wBkf2zPwWp4AW5ywZK2Rd5BA7G/BaatFS1PnfazeYV3oU/9Y+iFde5UpVVHhVCe+i4JyWeSyw5lkdFIxjRCR7DeiB72f3rtlqsm7JzVlgQ9kNs95ImfElbVXVOOWCRg8VV6WtOfNsiTlqt04lngAjotdEI3uOa3H8rlwVlb36zsXTcodgtMW8IruZilvQa05ji3MDB5LgJEE5x3EkaVqoN0RgP7MSXsOx8FXV7ub0NLs3JGjFXW7muJj0qlaLM+qo/oYk/Ydh4J9VRvQxN/Qd8FBlfItOlhzXijDprO1UJ+QWb9VxvQxNwzHfBYtqguYZPaWmU+kCMO1eWZ6pxe5kuZO5RWKBZYMJ1qhBzWNzsZSccXTA3mS2X2ysHrcLvXz3HPdVWq0T8cQ0rWM7PZcJSbcnr2H0bZ8qrFEe2Gy0w3PcQGtBmSToC3agfkpsJi29jpGUJr3k75Zo7JuU8JilNzjdlVi6EaFTJF30LNpjthsc95DWtBc4nQABMk9i032ysHrcL3lquVa3c3d72ggGI5jNMjKeeZe7LtUFT7lxVrOErJDGEwMa8HOTa1Por7ZWD1uF7y5flFyssz7E+FZ47HviOY0huJzZ5xO7QFD1aIBxtUUsQ2mrDtPZkITUsz0dzLsvgs7gBY9mb8gsjglIy3mgpq0So/krv8hX/1G+yKLgBp3VXe5Dt/qM3NFE3gvml2fUpdvP8A84LrfoSe3QiM0IrAzRDGX8HNtOd5zAZ9Ux+y5mlV3vKTY8GxPNcWnqMyPEf7lwVaKpxMbVH4mz2XUz4WHVp4P2sK0Xl5wNV64JVuNooFAPvcT5kvb2x7LBit0FjQdzm9Fw7CCFt1AuRmWL7A8tc0vgPM3tBE2nRnNnhOWkYTkMQpWseW9giNDhaobdzzzbh+V0j26Fb06qlHV6mKxeDqUpuyuuDOkRaT7W2H1yB+o1PtbYfXIH6jfipcy5imSXJm7RaT7W2H1yB+o34p9rbD65A/UajMuYZJcmbtQdyiXkI1uiYzbDlCGEvInnDseX4qUo2V9ia0uFrgukCZCI2ZkNAx0qAotpdFiOiOJLnOc4k6ZuJJnvxSuKknFIt9j03GrKbXC3j9ke3QJ9dFT6MOyqyWDDdr3qpPyCr8zNMqaZI/I5dubDjRyB0nNht2gNGc7HYc5nuqSVosirEYNigMM5lmeZ6QXkvl2Z0uxb1W9GOWCRicZUVSvOS3X07FoiJeWu3TfZ4E9DXRCPaOa3H8r1HUGBProt5l7bufvCMdTXc23GYAhjNMtxIc78y10AYbqqvrzvJs0uzqKjRinyv46ln6N3VVRZvkFlcAJwSoMzLHo4nmG2X7leuOtKVStFySJWPTBN0tdFIuQ8Ppk7AuAsDJvGwYnepOyIgdEu2lWGCjaLf5oZnbtS9SEFwV/F+yO3ZoRVaidKI5HK2xc5DezaMOsYjxAUQOb81PF8QZhRDlbYOajFwHRiTI3O/F8e1JYyF0pF7sPEZZyovjqu1b/L0NJwAqOHfrOxVrrOxKVVeaYwLRC7qrGPG5bZzJ9dFYdZx81Ipi06Wt0a9OAFnfRvkE+jfNdZkR9FIwZpNZv0buXr6N30RmR70UjAn30WTZW91Vc+jcbVfhQpfsF45Kx1Cm76lwfwFk3fZjFiw4QlN72MmdHScB+6x+CV1fJpYedtoeR0YLXO0TBcQGtB2eWSPZXNOOaSid4mr0NGVTkn48PMl+DDDWhrRINAAGwDAKxelrEGDEikgBjHOx0dEErMXG8qlv5q73iZBiuZCEt5LnA7ixjh2q5lKybMNShnnGHN2IRY7OcXHWST1kzWewfILCsrfkFnD+SqabNzRVkV4JSlUpVK0XBOK0SlUpVemNJIGvUEA2krs2N0wScduHfp43KW8mbNmQ2jcuAybsOc9o1NUp3dCk0K6pwyQUeRhMVX6etKpzenZuXkZqIi7FyxaocwuFynurnWOZoOlp2OH7alIBC0d8WWeK8aTVmdQnKElKO9aog+IwtJBBEjIjXMLzWi6zKu59MZgxHljd53Zr+WPJ01naqerTdOWVm4wmKjiaSnHvXJ/mtxwTtTgBOAE4JUYyOCdiUqqUqq1ogBWicdapSqrwAgBwAnBKcEqlKoAUqpO5JbDKFFjkYvc1gx1NGdKWrFyjI+NF0dy8oT7HAbAZAY4AuJcSQXFziZkAacQOxMYZxjO7K7akKlTD5Kau21fVLTfx67E2KI+Wm3Z0WBAH4WuecdbyAJjcG/7l4+96N6sz3nfBcZlDfb7baHR3tDSQ0SGIaGiUp69femq1aMo2RS4HBVadZSqKyV+KfoWLK35lZVKq3AGG6quVoq5mpgrIVoqUqq0qnAC8OhwAtjdtn16zgP3KxbJAzju1n4Lr8n7uziHEYDQncJSu877ih2xjcseghvfzdS5d/p26dFktd2aBPSV2kJsgtfddmkFs1YGaCIiACs2iFnBXkQByF52SRUf5RXHmExIY6Oto/CdvUpit1lDguWt1kzScFHVpKpGzGcJi54apnh3rg1+bnw8URNwSqUqunvvJ6U3whhpLB/8APw7ti5pwx30VVUpSg7SNjhcXTxMM1N9q4rt/NSlaJTWdqUqnACjGRwAnBKcEpSqAKUqq1olaJSqAPLtG6qwI7TOi2J/gLwWfMrpOxHUhmRq+aPxKuwYSz+aHGtVDO+i6cyNUbMMHfReqVVKVVeAFGMDgBXbPALzIdp41L1ZrIXdWs/sF0d1XVnSAEm1TVDDOest3qVG0NqRoJwp6z8l9+rx5PzdF250gB0R4qQLmu+UsMAsW6bt0ADBdTZYAaFZpW3GUbbbb3l2G2QXtEQeBERABERAFCFgW6xBwWwRAHF2yxlpXPXrcbI0z5L/OGvrGtSVarIHLQW27SNC8lFSVmd06k6cs0HZkT2+64kE9JsxqIxCwiPmVKEaz6iFpLfk7Dfi0ZvVoPYkqmD/o+5l9htucK8e9fVez7jiqVStFuLVk/EZiJO8Frn2N7cMx3dOiVlRqR3ot6WPw1T5Zrvdn4OzLFKpwAqlu6Z1BM3r3lRDad9xTglKat6qR1y1b17bAcdDSex0gvUm9xzKpGPzNLtdi3WiUqspl3uOoDr0+C2FmuYnUT4BTQw1SXC3b+XEK21cLT/lfs1893mahkMnQMdmzrWxsd2knHEnUFv7LcktMgNgW7sd3gYNCcp4WEdXq/Io8VtetVWWH7V1b/H28TU3dc+gu7Auou67pywwWZYbs2reQLOGhNFSeLJZg0LKREAEREAEREAEREAEREAFbfCBVEQBr7VdoK1FpushEQBgRLKRqWNEsoOloREAY0S7WH8Ksm5IexEXoWQbcrBtVxl1M2Eoi8PLIyYVgaNDVmwrEToCIg9NjZbqJ0rcWa7w3UiIAzWsAXtEQAREQAREQAREQB//Z",
                              "size": "Small"
                          }
                      ]
                  },
                  {
                      "type": "Column",
                      "width": "stretch",
                      "items": [
                          {
                              "type": "TextBlock",
                              "wrap": true,
                              "text": `${question}`,
                              "separator": true,
                              "size": "Medium",
                              "weight": "Bolder",
                              "color": "Default"
                          }
                      ],
                      "verticalContentAlignment": "Center"
                  }
              ]
          },
          {
              "type": "TextBlock",
              "wrap": true,
              "separator": true,
              "text": `${answer}`
          },
          {
              "type": "ColumnSet",
              "columns": [
                  {
                      "type": "Column",
                      "width": "auto",
                      "items": [
                          {
                              "type": "Image",
                              "url": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAnFBMVEX///8AAP/8/P/39//9/f/5+f/6+v99ff/y8v++vv/w8P/19f/Q0P/U1P/p6f/b2/9DQ//j4//n5/8yMv/Ly/+MjP+5uf9KSv/Cwv+Cgv+Hh/9paf+1tf8qKv+oqP/a2v+UlP+Zmf8fH/84OP9bW/9zc/8jI/+fn/9mZv+kpP9HR/+trf9PT/8QEP9ubv9hYf9XV/+Xl/93d/89Pf8rEww7AAAQc0lEQVR4nNVd6WKiMBBWEEErIHKIiop44IVaff93W49uq80kmSBXv5+7NWRImCvfTGq1nCEpivwxDeLQHaiq6rrubDbZeS2nY7XlKxRFynsG+UFq96zpbrAf1+lI5id1Z1rGR9mTFYZidfzJssuQ7RnRyQ0c3Sh70ngYzi48sVYOxHy58abNsueOgLUbruei0v1fy70687WyJWCiN1n2o5TifcFeqC2lbDkokLyjnbwn3tdSdodW2cKQkKaHLIT7hr3RqmRGlF7Qz1S+Ow5Ou2zBvqB1Pu3s5bvh6FVht2r+cJWPfDfsN3qjZPkCNUf5bui7TokfZMM/5CzfDZeBWZaAZhHy3WC7pfit2rkY8e5IdkVv1YYxK1C+Gy5eoe5cO8CGDRli5BTnzemquHeW2N396XQ8rkej42nfnY/F/Vd71itGPnkj5MBE28Pgcxe0/M7UNC2r1zMsc9pxfN/bzQbrrtC7WvtFCKgv0ZNKtufY101Do2yvptYzdWezvKBFtMPc42Rlg53OZehbhoYIaSXNsFoDrON3ynkZrRFqGskxMMTUe0MyNnvc2HkaR8lHvOnoonZSjq+0DnPEJ7DIzcfRNnzxtup7dqu9O/A/g3FOrqo14D3ZPk8yeL1OuOAtZDLJI3R0jpzHXtysskhWcOZ4vEn23rjC82K6k6mc3ePanZDjERyd7J52gxyzX+ol6GX8acjTIVvES6ZmQ/pkPiyJ8/gspCnbNEUZivihsp40dnPLGOlHps7ZZfWcNitROD5Ps3oOhNaJJeMkm2jDYgiYHL1MnkFHO2TZx1kW22e6pj/gEud/YNRwGK5+4r4/gSnDDB6KSYMZE/pRT/K2EmC42smmqJw068gg+XzvW9SW1KGjInO1zZi+kzbvDCzTBVwWfKjg0MOaN4yGRPW17UnhiWhzRFU4rbRjKtStsS8kX/ILRkjzHNN6N9KE9tLWerZzR0IOaKbxks4N92hkg0FpZ14OTcRTGsdKp6VNwhK5BPqWMilV3PIbNDUaZxgGimN6ok1LlKgih/BASfFK9BUWzYkU1TYBPMx4V/KR7HVzUc68IrFPsQMr5goIeI02BrCO34v4IAZ8MhGVvUUfaFPyGwOB2VF8mbgiZCWNknMI0CO04G0QVoZX14YVfR+bYjRgS6iWaiZe0YM1Knafwtt8WSnSoAXaxRVun8LeWtHREg+wd3PC7FMLTFugflooWmDAiDh6k8Hkb7YJ5mwwgfL+iEDKgfZoRQzhKxTQsTzxXPAPMOczqJAa/YG2gObKS9v40I+E/KECYUF2O+LofDDErALLEwQYHwyZP5lAP6mglvmC5IouiAaFFCIObdEwoE/xwPgBpJ22lbOEz4DYIWN6XsoEzgeiwnmPQpBDQNscqMoGckjPFdWj/2EB+3RMO/LrAGSEblr2T2EIAN1BWxbgK0xmxU43BSTgdGwMq3/IW1/8gcLAKbCIQ2jeEnBKkeR9hp0JAPWxgj4uC4jsj4XPNg3aQJARAiklj/yzeuXVzAPA7huTjk0b+GD/xhJe5w5oEPLYVAeWsJxDtBQAPPAL4agAn6tamewhDz0gg/17eRTyT5JcyU6ZorEjp7/+9TeAnhlUKn3IhgkYgl8mkVzmpLphIQnImMcvf9EGVrmgApVsAPjU+5c/AF7BW0ScwqGQh0njl8CW3MZ9UVNherE7dMNAf8uVtVqz4WDgTjqiowREFvQlajDJJOlQKPCVg1F3fI9FV/NTmFYJN73D9jFKND8NxUYxyIOM0dN/z4hA2RbiGfn7lwGiQ6rsXGv7OspISEbSoHeftiHpsR0FcsA6EGbPhHPIPeA0aSjAIdHJffijTYGwIsQPDZcKHcUSWBJ4miDEBCLfs/r9pZFf6RivZwJKYcRJRFVJNAKWjR+FzPX+HJiR6Ys9a6gXUE7E62IFWD6VzI3X6Qbx2/F/ZQLQZEPssDqjlGaJ/oqoTKe6iOdBvqXJ1/8A/gD2xRnMoo8Jkn3TZtVzJFC4DoLciv9dazK6SpBj1qgEzTtWyEV0WIPU51hqpUn89IucIZGyn5FjQunYZ6ioUdqMPXoDNshpkEr98XJ65E7Dmvsdr1AQNTf2El4VBjJd1CCN/iNXOCU/Q2Qmn0ai+wGK/cGsqrphg0w2kEHug7jgEAvRRfqkOrep1+9AG0KD2/hshHzjFvHLxV0TtIh/HyJ1IPnL37ARo5B2jBgF6eYaxG5c3X6pzIgRkZluCTwxfkGC2F8d7ihY4wUw+m5eH5AoRb4yGo/4WULE/qKQdZ+BTKhIZELqtlgWoWPnSDP2AZ6iv0qI8NzY9anf88SA3A6uBNlJ+hGquISI7TDLTkKdWK1jE7JGITK2w+xSxMvKbpcCYaCtQEEHlmrbRPRYQNgdnsGvP/QFBkAJhQxRUtAlNnxrccFMizuKjQ0vgF3VrkkEkW2Fjsj4Fh/lmHLbieC5raQy1WtNwnnuoiNXZtRzB+oD4rQWuEZh6Lwf6bcFNYXwA474RBlXSaB0FnSw9wyBTIZPnOnHNYXYIwIMGpMTPeGYHDDj8wdY3X6FQ3w3w5pCJJJEeviAPN1v2EjXocMMwkQoPTpxwqTWZGL0WCDbzWy3gM5iaKwvMYkFKpHITXWVkBhSqHBYZ3Q8UNHbHWbPPyBUJEBG85CE+NqaG+j2WkBjgYSzB/ZCBwRtIiaHJBRkCdHM/kJoarRidIA1wgJ5yAZIKMyD6oAx+lqwbLUHeg8nQWYkSsJIuLrdOBMaNRLb6ncsiVFWoSi3VSJ0FiRhivN7//C8yaJ+uo4j+vJ5HZNUvWcJLxuQcJWGoSD54eNwM5qvh5O0DACpMzvv7aieRPZejVPxlTASUriZXGgdP9jtPOe9hjHK1Pd2gedPU5bjEsFFhhJWAxgJs2yhVTwwu/RPS4jTpal7oVQAMhGxZuDTVApkC+73/dJqgUxFvR1bVAzkmTQUH/4tQtsryCNbtaYQmQ30uXkFAcX4ZJ4GH7hWD2QQNgRybX+LWPoKkt2zAfKlYlF1tUCmN72aROhXgfRk5UAewujQucUfqZQBAJxbaFCq/++6bQaZ3JShZFlVGu2IwyTKg+YKdAZ8/kOFFq/QiVTPulmr9QgT0v1DNxG+guwIETbAxPxfNRdAK0sP/ue/GgMDRNl7uo5MWrtlTzUlDCKrfOdEAby2fqWbDNAxJdbqwWtLz02sGkiPxr0frgJm8m/afKAy/5GRAa4FwHKEqwWJPEr+Is68wfOuFEjGw/aLaiTI1S9VDbHYnORZ+X+GOGBGWPbCLy+6kn0W/4s8x/xfbyFYM+PsUx8uvYmpu2A8mbQVq2+NSRYdMenxcX3kl7BVtd2eGbqSge5P3VOPrF1jbdOPYd0uvuN15xCxk9VksfZP7VqNtIgLltG/WVB7wviD7KHdbqydsfQMQCd4quaOCUtCbUR0x723Zr9T2FZtercJgk1nvkGWOj/XkArXAXfu+1rVC+k22Pbv+cAzM2wFqkSf64BrZOURp+7Pv9vQlavnnvFo+w9jxqktJhl2rx2gAD4z5/jiqzb2Euq59gfRWupj6pyqW8Cmv9bjp+ip4H+9tK3r5LZX298XsPE6xALFyL9sOrmLE16A8c1WnS9buchoTI7/H8HLwzeBoo3XvhgQO41LCvwpv16dsr9Vx3R/bmE98ZwoE2D+/dK8CkBi5ea+n1325Jyl0tFaz5vqwEv+QRftEGVzQP3LiDtl86Wtdn8zbWdgIhXDGT5br8Tl5m/JyiYgPCL9VkzJWO/X3ceLTcd4ayk/LD983XHjmMvKbgBLOCfetQbwmRf8GbXjX3YoWs+8abqPUjI6O/e3yut7fGtkAPxPwNhBTFhE9YzkER95sj/PfEus/UpDmwYhcMnqCBOMAop0BShfqI87YhFrNR2qybdPh+FOx63l9cOLB+s+RPtHXeFsAFoSrGAA/JoExa0xZnBNQmT3j4ON02MYS81szZaLrg2XI1xwdhaot45AOzAF+mXtcYc0DuM65iSyt4vlIIwnu9Z/eLvJLByMTl9deyg441IJDvB+KSX8M+ApIeohNTnkFmSLoo9kn30AXxeNBQw1MEUXrOgHzhW+YpijidRQ2Q61sAmqekWza+SAfS2qCMYqugYS+rbo4bsFFMBEAZqPbO04hV5IROcW+hRag1aFUcgMXZbXx+ecGtYEvmRICKOWwBl0C/j+WTxuGdIXIn3NJGNHuxkNibUjVOcEKXFWT3a4ZFLsJEprcZqxsCAYoIj31a/VwBUQTXFbYTeF0lmddqIkkBR3I8ClaAvhcOGjpe5FLGQyX7jiN7eSRJkrIs4u/wDvipqlCPt6nnvYopZydfXUnRQcHrg3Ucz7WQcqk2Pnh+lTuEZDS9Cj/hn5pF6jrVTNJBXwI+TeM1OTZ9Dv+mnZfLIxdYJ4sJj/ljOJuiM39hwzdVog7V1BlIpVwSaBr1A0wzI7/tXf3sSfs3hy9cEd3bSMj3dSrfB9T6i67/zu7Go0pBsajQxuUYTrapEXb8FNWarF5zNB5wmrLyh351Xp0idK8Tfa/aLdf1gZ3imlmaEAQ512h2VFVpGsSb9DpEa7De/TqBoiGpSWKkKEQ8pdsqtNBTYqTUDB+1QofVmi8jngPcpl02NBi63QeozNShaR1kJDvEk+teujSHOX7AGlZe4CprihkXq3epl3JlA7hAzSuFyw43eFWhbTveHTWm+JdGV+Go/af/VQzt0eHxPaO++ik4+voPe2PJXB5jdcWqS5SingdRWpjRHt4q9eNUfUhEFqAcFbI/7jUDDbnezi9Y23is9pt5hfkTgFLiOceXjM483ac6DX4jc+i6qPaur0Tlnve1kWo1/Z2i/EEzeoOvQqYPi+dTYZq2i7BVxS6izpOcnoMwt10GP1fzzl3YHB+GTcTZBssvGvGOrmduNKrtT9YM9KKgdZhQEKs//mapCbxtEXzKR5hl1YpA3zSQmKFCL8UJYGqDOv/U3ztAn7nMXeWBnHjZpOddIe2GYq4I36xDndteMsSd9tZ8A+1ElQZCkxdDh9VOtz18/Ik+t5ZBe/XwK6eURwPW6DcvsQvx9XNZ3PNUe+ehTkE4XLO96T60n33Hrv3rX4yG0wXb/k1tei4TD6sX6/38s5bfT4EYwoDLcXjPLMMfS4t5I8cPTaktABU0OauojXV88/Ld1As0kuQ98yPhARltI2TO+M5Iwlx4yNBABTRfMskr46cXTL0CjL2dR6ZqcVrrlN2b8xnxUReEs7BteSxGq/HMZB4LV8x+k84Di+3/KC3WwwEqOljPJfwAemQ65SJZBEY3vefeAyH6/ER6jP4+JakryWQxSEZafQG5jb7G7s2WPrF36aIHOvNMgQUVBKjbx1zpz6DGMucHdAxnDOeEWfGpdhmR2B5NYgZxn7YdnN1RQ/5LvKqbHfpG0KnamM05iRDnsHi6AqHauahs+4viEt1E6lGgFJ9LOvVLAnlSC2/IJzvmQi5Wo7LCCVng5X1Xp600baR7co9zol2t6nuk+5lOPFIC6ja4ow5Km3WYqy9Vf74cS3KmAasDCmTms2wnkDyfY88TtmqTyddFDalqUH7vpCXc6ke/hsmVZP+wtbkwqpqcjyh+UEceiqDwzccON1LFmWlWbu+/Ifxfsxl7YNo/IAAAAASUVORK5CYII=",
                              "size": "Small",
                              "width": "25px"
                          }
                      ]
                  },
                  {
                      "type": "Column",
                      "width": "stretch",
                      "items": [
                          {
                              "type": "TextBlock",
                              "wrap": true,
                              "separator": true,
                              "weight": "Bolder",
                              "text": "I hope you satisfied with this answer !"
                          }
                      ],
                      "verticalContentAlignment": "Center"
                  }
              ],
              "separator": true
          }
      ]
  }
    return card;
  } catch (error) {
    console.log("error", error);
  }
};

