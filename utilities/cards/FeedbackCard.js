module.exports.FeedbackCard = (message) => {
  return {
    "type": "AdaptiveCard",
    "body": [
        {
            "type": "TextBlock",
            "size": "Large",
            "weight": "Bolder",
            "text": "Feedback",
            "horizontalAlignment": "Center",
            "color": "Accent"
        },
        {
            "type": "ColumnSet",
            "columns": [
                {
                    "type": "Column",
                    "items": [
                        {
                            "type": "Image",
                            "style": "Person",
                            "size": "Large",
                            "width": "280px",
                            "horizontalAlignment": "Center",
                            "url": "https://qubisastorage.blob.core.windows.net/files/courses/608/images/img480/608-20201006105027824.jpg"
                        }
                    ],
                    "width": "stretch",
                    "horizontalAlignment": "Center"
                }
            ]
        },
        {
            "type": "TextBlock",
            "wrap": true,
            "text": "Thank you for your valuable feedback !",
            "weight": "Lighter",
            "color": "Dark",
            "horizontalAlignment": "Center"
        }
    ],
    "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
    "version": "1.4"
  }
}



