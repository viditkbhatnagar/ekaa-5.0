// const {TicketResolvedCard} = require('../cards/imageBase64.js');

// module.exports.ticketResolvedCard= (id, title,raised_date,resolved_date,ticket_status) => {
//     try {
//         let card = 
//         {
//             "type": "AdaptiveCard",
//             "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
//             "version": "1.4",
//             "body": [
//                 {
//                     "type": "ColumnSet",
//                     "columns": [
//                         {
//                             "type": "Column",
//                             "width": 50,
//                             "items": [
//                                 {
//                                     "type": "TextBlock",
//                                     "wrap": true,
//                                     "text": "Your ticket has resolved successfully",
//                                     "size": "Medium",
//                                     "weight": "Bolder",
//                                     "color": "Accent",
//                                     "horizontalAlignment": "Center"
//                                 },
//                                 {
//                                     "type": "Image",
//                                     "url": TicketResolvedCard,
//                                     "size": "Large",
//                                     "horizontalAlignment": "Center"
                                   
//                                 },
//                                 {
//                                     "type": "ColumnSet",
//                                     "columns": [
//                                         {
//                                             "type": "Column",
//                                             "width": "stretch",
//                                             "items": [
//                                                 {
//                                                     "type": "TextBlock",
//                                                     "wrap": true,
//                                                     "text": "TicketId",
//                                                     "weight": "Bolder",
//                                                     "horizontalAlignment": "Center"
//                                                 }
//                                             ]
//                                         },
//                                         {
//                                             "type": "Column",
//                                             "width": "stretch",
//                                             "items": [
//                                                 {
//                                                     "type": "TextBlock",
//                                                     "wrap": true,
//                                                     "text": `${id}`,
//                                                     "horizontalAlignment": "Left"
//                                                 }
//                                             ]
//                                         }
//                                     ]
//                                 }
//                             ],
//                             "spacing": "ExtraLarge",
//                             "horizontalAlignment": "Center"
//                         }
//                     ]
//                 },
//                 {
//                     "type": "ColumnSet",
//                     "columns": [
//                         {
//                             "type": "Column",
//                             "width": "stretch",
//                             "items": [
//                                 {
//                                     "type": "TextBlock",
//                                     "wrap": true,
//                                     "text": "Title",
//                                     "horizontalAlignment": "Center",
//                                     "weight": "Bolder"
//                                 }
//                             ]
//                         },
//                         {
//                             "type": "Column",
//                             "width": "stretch",
//                             "items": [
//                                 {
//                                     "type": "TextBlock",
//                                     "wrap": true,
//                                     "text": `${title}`
//                                 }
//                             ]
//                         }
//                     ],
//                     "horizontalAlignment": "Left"
//                 },
//                 {
//                     "type": "ColumnSet",
//                     "columns": [
//                         {
//                             "type": "Column",
//                             "width": "stretch",
//                             "items": [
//                                 {
//                                     "type": "TextBlock",
//                                     "wrap": true,
//                                     "text": "Raised date",
//                                     "horizontalAlignment": "Center",
//                                     "weight": "Bolder"
//                                 }
//                             ]
//                         },
//                         {
//                             "type": "Column",
//                             "width": "stretch",
//                             "items": [
//                                 {
//                                     "type": "TextBlock",
//                                     "wrap": true,
//                                     "text": `${raised_date}`
//                                 }
//                             ]
//                         }
//                     ]
//                 },
//                 {
//                     "type": "ColumnSet",
//                     "columns": [
//                         {
//                             "type": "Column",
//                             "width": "stretch",
//                             "items": [
//                                 {
//                                     "type": "TextBlock",
//                                     "wrap": true,
//                                     "text": "Resolved date",
//                                     "horizontalAlignment": "Center",
//                                     "weight": "Bolder"
//                                 }
//                             ]
//                         },
//                         {
//                             "type": "Column",
//                             "width": "stretch",
//                             "items": [
//                                 {
//                                     "type": "TextBlock",
//                                     "wrap": true,
//                                     "text": `${resolved_date}`
//                                 }
//                             ]
//                         }
//                     ]
//                 },
//                 {
//                     "type": "ColumnSet",
//                     "columns": [
//                         {
//                             "type": "Column",
//                             "width": "stretch",
//                             "items": [
//                                 {
//                                     "type": "TextBlock",
//                                     "wrap": true,
//                                     "text": "Ticket status",
//                                     "horizontalAlignment": "Center",
//                                     "weight": "Bolder"
//                                 }
//                             ]
//                         },
//                         {
//                             "type": "Column",
//                             "width": "stretch",
//                             "items": [
//                                 {
//                                     "type": "TextBlock",
//                                     "wrap": true,
//                                     "text": `${ticket_status}`
//                                 }
//                             ]
//                         }
//                     ]
//                 }
//             ]
           
//         }
//         return card;
//     } catch (error) {

//     }
// }