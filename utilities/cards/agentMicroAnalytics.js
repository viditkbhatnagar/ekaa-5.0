module.exports = {
  agentMicroAnalytics: (inprogress, closed, onhold, newticket) => {
    return {
        "type": "AdaptiveCard",
        "body": [
            {
                "type": "Container",
                "style": "emphasis",
                "items": [
                    {
                        "type": "ColumnSet",
                        "columns": [
                            {
                                "type": "Column",
                                "width": "auto",
                                "items": [
                                    {
                                        "type": "Image",
                                        "url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRtJfvJ1Ekgwl783Uer2ALrVkmu4GOS-jua9eJyhEZvJHM0djcYInr_W17uW3yWhmWwElU&usqp=CAU",
                                        "size": "Medium"
                                    }
                                ]
                            },
                            {
                                "type": "Column",
                                "width": "stretch",
                                "items": [
                                    {
                                        "type": "TextBlock",
                                        "size": "Large",
                                        "weight": "Bolder",
                                        "style": "heading",
                                        "text": "Bot Analytics"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "type": "ColumnSet",
                        "columns": [
                            {
                                "type": "Column",
                                "width": "50px",
                                "items": [
                                    {
                                        "type": "TextBlock",
                                        "wrap": true
                                    }
                                ]
                            },
                            {
                                "type": "Column",
                                "width": "stretch",
                                "items": [
                                    {
                                        "type": "TextBlock",
                                        "wrap": true,
                                        "text": "Private to you ",
                                        "color": "Accent",
                                        "weight": "Bolder",
                                        "size": "Small"
                                    }
                                ]
                            }
                        ],
                        "spacing": "None"
                    }
                ],
                "bleed": true
            },
            {
                "type": "Container",
                "items": [
                    {
                        "type": "TextBlock",
                        "spacing": "Small",
                        "size": "Small",
                        "weight": "Bolder",
                        "text": "AG1001210",
                        "color": "Accent"
                    },
                    {
                        "type": "FactSet",
                        "spacing": "Large",
                        "facts": [
                            {
                                "title": "Name",
                                "value": "Radha Kulkarni"
                            },
                            {
                                "title": "Email",
                                "value": "radha.kulkarni@gmail.com"
                            },
                            {
                                "title": "Department",
                                "value": "HR"
                            }
                        ],
                        "separator": true
                    }
                ]
            },
            {
                "type": "Container",
                "spacing": "Large",
                "style": "emphasis",
                "items": [
                    {
                        "type": "ColumnSet",
                        "columns": [
                            {
                                "type": "Column",
                                "items": [
                                    {
                                        "type": "TextBlock",
                                        "weight": "Bolder",
                                        "text": "Ticket Infomation"
                                    }
                                ],
                                "width": "auto"
                            }
                        ]
                    }
                ],
                "bleed": true,
                "separator": true
            },
            {
                "type": "ColumnSet",
                "columns": [
                    {
                        "type": "Column",
                        "selectAction": {
                            "type": "Action.ToggleVisibility",
                            "targetElements": [
                                "cardContent4",
                                "showHistory",
                                "hideHistory"
                            ]
                        },
                        "verticalContentAlignment": "Center",
                        "items": [
                            {
                                "type": "TextBlock",
                                "id": "showHistory",
                                "horizontalAlignment": "Left",
                                "color": "Accent",
                                "wrap": true,
                                "text": "Total Tickets [5]"
                            }
                        ],
                        "width": 1
                    }
                ]
            },
            {
                "type": "Container",
                "items": [
                    {
                        "type": "FactSet",
                        "facts": [
                            {
                                "title": "In Progress",
                                "value": "5 (Five)"
                            },
                            {
                                "title": "On Hold",
                                "value": "0 (Zero)"
                            },
                            {
                                "title": "Resolved",
                                "value": "7 (Seven)"
                            }
                        ],
                        "spacing": "Large"
                    },
                    {
                        "type": "ActionSet",
                        "actions": [
                            {
                                "type": "Action.Submit",
                                "style": "positive",
                                "data": {
                                    "id": "_qkQW8dJlUeLVi7ZMEzYVw",
                                    "action": "approve"
                                },
                                "title": "📃Show Ticket List"
                            }
                        ]
                    }
                ]
            }
        ],
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        "version": "1.4",
        "fallbackText": "This card requires Adaptive Cards v1.2 support to be rendered properly."
    }
  
  }  
}
