const axios = require("axios");
// const url = "https://ticketappekaa.azurewebsites.net/";
const {ticketApplication} = require("../config/config");
const url = ticketApplication.baseUrl;

module.exports.raiseUserTicket = async (payload) => {
  try {
    console.log("Payload", payload);
    if (
      payload.ticketTitle &&
      payload.ticketDescription &&
      payload.ticketIssue[0] &&
      payload.ticketDepartment &&
      payload.ticketDate
    ) {
      const resp = await axios.post(`${url}/ticket/USR1`, {
        title: payload.ticketTitle,
        description: payload.ticketDescription,
        issue_observed_on: payload.ticketDate,
        impact: "1",
        urgency: "2",
        department: payload.ticketDepartment,
        issue: payload.ticketIssue[0],
        attachment: payload.ticketAttachment,
      });
      if (resp && resp.data) {
        return {
          status: "Success",
          data: resp.data,
        };
      }
    }
  } catch (err) {
    if (err.response) {
      return {
        status: "Failed",
        error: err.response.data,
      };
      //   console.log(err.response.data);
    }
  }
};
