module.exports = {
    checkIsAgent : require("./checkIsAgent").checkIsAgent,
    getAgentDetails : require("./getAgentDetails").getAgentDetails,
    getTicketDetails : require("./getTicketDetails").getTicketDetails,
    getUserDetails : require("./getUserDetails").getUserDetails,
    raiseTicket : require("./raiseTicket").raiseTicket,
    saveUserFeedback : require("./saveUserFeedback").saveUserFeedback,
    fetchTicketStatusOfUser : require("./getTicketStatus").fetchTicketStatusOfUser,
}