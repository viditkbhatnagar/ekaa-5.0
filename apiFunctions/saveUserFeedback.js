const axios = require("axios");
// const url = "https://ticketappekaa.azurewebsites.net/";
const {ticketApplication} = require("../config/config");
const url = ticketApplication.baseUrl;

module.exports.saveUserFeedback = async (email, feedback) => {
  console.log(email, feedback);
  try {
    const resp = await axios.post(`${url}/User/${email}/addFeedback`, {
      feedback,
    });
    if (resp && resp.data) {
      return {
        status: "Success",
        data: resp.data,
      };
    }
  } catch (err) {
    if (err.response) {
      return {
        status: "Failed",
        error: err.response.data,
      };
    }
  }
};
