const CronJob = require("cron").CronJob;
const { sendProactiveMessages,getManagerReference,getCallerEmail } = require("../services/");
const cron = require("node-cron");
const { Analytics } = require("../cron/getAgentAnalytics");
const {agentMicroAnalytics} = require("../utilities/cards/");
module.exports.CronCaller = (context,agentData) => {
   try{
        // console.log("CronCaller",stepContext,agentData);
    // console.log("Running Cron Job",context);
    console.log(new Date().toLocaleTimeString());
    new CronJob(
      "32 19 * * *",
      async () => {
        //   console.log('inside cron job',context);
            // console.log(context);
        let inprogress;
        let closed;
        let onhold;
        let newticket;
        const data = await Analytics(agentData.AGENT_ID);
        if (data.status == "Success") {
          console.log("here");
          inprogress = data.inprogress_ticket;
          closed = data.onhold_ticket;
          onhold = data.closed_ticket;
          newticket = data.new_ticket;

          console.log(agentData.AGENT_EMAIL);
          const fetchAgentManagerReference = await getManagerReference(
            agentData.AGENT_EMAIL
          );

        //   console.log(fetchAgentManagerReference);
          if (fetchAgentManagerReference) {
            if (
              fetchAgentManagerReference.userId &&
              fetchAgentManagerReference.convId &&
              fetchAgentManagerReference.aadObjectId &&
              fetchAgentManagerReference.tenantID
            ) {
              let manager_reference = await sendProactiveMessages(
                fetchAgentManagerReference.userId,
                fetchAgentManagerReference.convId,
                fetchAgentManagerReference.aadObjectId,
                fetchAgentManagerReference.tenantID
              );
                console.log(manager_reference);
              if (manager_reference) {
                // console.log(stepContext)
                // console.log(context.adapter);
                console.log(context);
                console.log('Done');
                // const adapter = context.adapter;
                // await adapter.continueConversation(
                //   manager_reference,
                //   async () => {
                //     let ticket_status = "New";
                //     await context.sendActivity({
                //       attachments: [
                //         CardFactory.adaptiveCard(
                //           agentMicroAnalytics(
                //             inprogress,
                //             closed,
                //             onhold,
                //             newticket,
                //           )
                //         ),
                //       ],
                //     });
                //   }
                // );
              } else {
                console.log("Something Went Wrong ! ");
              }
            } else {
              console.log("No Manager Reference found for the Agent");
            }
          } else {
            console.log("Error");
          }
        }
      },
      null,
      true,
      "Asia/Kolkata"
    );
   }catch(error){
         console.log(error);
   }
  }